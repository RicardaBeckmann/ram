"""Loads settings from config file for m2m.py"""
import sys
import os
import re
from glob import glob
import ConfigParser

errors = (ValueError, ConfigParser.NoOptionError)

def load_settings(parser, config_file):
    """Loads settings from config file for m2m.py"""
    args = parser.parse_args()

    config = ConfigParser.SafeConfigParser({\
        # setting defaults - general
        "geometry": "1,1",\
        "outputfile": "multi.mp4",\
        "fmin": "",\
        "fmax": "",\
        "dir": "",\
        "namelist": "ramses.nml",\
        "cbar_width": "0.1",\
        "vr": "False",\
        # setting defaults - maps
        "proj": "1",\
        "kind": "dens",\
        "logscale": "True",\
        "min": "",\
        "max": "",\
        "bar": "1 kpc",\
        "timer": "True",\
        "label": "",\
        "labelcolor": "white",\
        "colormap": "gray",\
        "colorbar": "True",\
        "sink": "True",\
        "true_sink": "True",\
        "streamline": "False",\
        "fontsize": "20",\
        })

    # reading the file
    config_exists = os.path.isfile(config_file)
    if config_exists:
        config.read(config_file)
    else:
        print 'Config file "{}" does not exist! Aborting!'.format(config_file)
        sys.exit()
    del config_exists

    args.config_file = config_file
    # General parameters
    args.geometry = [int(x) for x in config.get("general", "geometry").split(",")]
    try:
        args.step = config.getint("general", "step")
    except errors:
        args.step = 1
    args.namelist = config.get("general", "namelist")
    args.dir = config.get("general", "dir")
    try:
        args.dir = config.get("general", "dir")
    except errors:
        args.dir = os.environ["PWD"]
    if len(args.dir) < 1:
        args.dir = os.environ["PWD"]
    try:
        args.fmin = config.getint("general", "fmin")
    except errors:
        args.fmin = min([int(re.findall(r'\d+', x)[-1])
                         for x in glob('%s/movie1/info*.txt' % args.dir)])
    try:
        args.fmax = config.getint("general", "fmax")
    except errors:
        args.fmax = max([int(re.findall(r'\d+', x)[-1])
                         for x in glob('%s/movie1/info_*.txt' % args.dir)])

    args.fname = config.get("general", "outputfile")
    args.cbar_width = config.getfloat("general", "cbar_width")
    try:
        args.smooth = config.getfloat("general", "smooth")
    except errors:
        args.smooth = 0.
    try:
        args.roll_axis = config.get("general", "roll_axis")
    except errors:
        args.roll_axis = ""

    # RISE only
    try:
        args.map_config_idx = config.getint("rise", "map_config_idx")
    except errors:
        args.map_config_idx = ""
    try:
        args.image_indexes = config.get("rise", "image_indexes")
    except errors:
        args.image_indexes = ""
    args.rise_fname = config.get("rise", "outputfile")
    args.rise_geometry = [int(x) for x in config.get("rise", "geometry").split(",")]

    map_name = "map%02d" % args.map_config_idx

    args.proj = config.getint(map_name, "proj")
    args.kind = config.get(map_name, "kind")
    args.logscale = config.getboolean(map_name, "logscale")
    args.min = config.get(map_name, "min")
    args.max = config.get(map_name, "max")
    args.bar = config.get(map_name, "bar")
    args.timer = config.getboolean(map_name, "timer")
    args.label = config.get(map_name, "label")
    args.labelcolor = config.get(map_name, "labelcolor")
    args.colormap = config.get(map_name, "colormap")
    args.colorbar = config.getboolean(map_name, "colorbar")
    args.sink_flags = config.getboolean(map_name, "sink")
    args.true_sink = config.getboolean(map_name, "true_sink")
    args.streamlines = config.getboolean(map_name, "streamlines")
    args.fname = config.get("general", "outputfile")
    args.fontsize = config.getint(map_name, "fontsize")
    args.lenbar = config.get(map_name, "bar")

    # load from file
    if len(args.lenbar) > 0:
        barunit = args.lenbar.split(" ")[-1]
        if barunit == 'pc':
            scale_l = (1e0)
        elif barunit == 'kpc':
            scale_l = (1e3)
        elif barunit == 'Mpc':
            scale_l = (1e6)
        elif barunit == 'AU':
            scale_l = (1./206264.806)
    else:
        scale_l = 0.

    args.scale_l = scale_l



    # override config file values with the command line args
    for k in xrange(len(sys.argv)):
        if sys.argv[k] in ["-o", "--output"]:
            args.fname = sys.argv[k+1]

    return args
