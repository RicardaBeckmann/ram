"""Loads settings from config file for m2m.py"""
import sys
import os
import re
from glob import glob
try:
    import ConfigParser
except ImportError:
    import configparser as ConfigParser

ERRORS = (ValueError, ConfigParser.NoOptionError)


def load_settings(parser, config_file):
    """Loads settings from config file for m2m.py"""
    args = parser.parse_args()

    config = ConfigParser.SafeConfigParser({\
        # setting defaults - general
        "geometry": "1,1",\
        "outputfile": "multi.mp4",\
        "fmin": "",\
        "fmax": "",\
        "dir": "",\
        "namelist": "ramses.nml",\
        "cbar_width": "0.1",\
        "vr": "False",\
        "dpi": "100",\
        # setting defaults - maps
        "proj": "1",\
        "kind": "dens",\
        "logscale": "True",\
        "min": "",\
        "max": "",\
        "bar": "1 kpc",\
        "timer": "True",\
        "label": "",\
        "labelcolor": "white",\
        "colormap": "gray",\
        "colorbar": "True",\
        "sink": "True",\
        "true_sink": "True",\
        "streamline": "False",\
        "fontsize": "20",\
        })

    # reading the file
    config_exists = os.path.isfile(config_file)
    if config_exists:
        config.read(config_file)
    else:
        print('Config file "{}" does not exist! Aborting!'.format(config_file))
        sys.exit()
    del config_exists

    args.config_file = config_file
    # General parameters
    args.geometry = [int(x) for x in config.get("general", "geometry").split(",")]
    try:
        args.step = config.getint("general", "step")
    except ERRORS:
        args.step = 1
    try:
        args.ncpu = config.getint("general", "ncpu")
    except ERRORS:
        print("ncpu omitted, setting to 1")
        args.ncpu = 1
    args.namelist = config.get("general", "namelist")
    try:
        args.dir = config.get("general", "dir")
    except ERRORS:
        args.dir = os.environ["PWD"]
    if not args.dir:
        args.dir = os.environ["PWD"]
    try:
        args.fmin = config.getint("general", "fmin")
    except ERRORS:
        args.fmin = min([int(re.findall(r'\d+', x)[-1])
                         for x in glob('%s/movie1/info*.txt' % args.dir)])
    try:
        args.fmax = config.getint("general", "fmax")
    except ERRORS:
        args.fmax = max([int(re.findall(r'\d+', x)[-1])
                         for x in glob('%s/movie1/info_*.txt' % args.dir)])

    args.fname = config.get("general", "outputfile")
    args.cbar_width = config.getfloat("general", "cbar_width")
    try:
        args.fps = config.getint("general", "fps")
    except ERRORS:
        args.fps = 30
    try:
        args.dpi = config.getint("general", "dpi")
    except ERRORS:
        args.dpi = 100
    try:
        args.keep = config.getboolean("general", "keep")
    except ERRORS:
        args.keep = False
    try:
        args.quality = config.getint("general", "quality")
    except ERRORS:
        args.quality = 15
    try:
        args.smooth = config.getfloat("general", "smooth")
    except ERRORS:
        args.smooth = 0.
    try:
        args.roll_axis = config.get("general", "roll_axis")
    except ERRORS:
        args.roll_axis = ""

    # Temprorary arrays
    proj = []
    kind = []
    sink_flags = []
    logscale = []
    r_min = []
    r_max = []
    lenbar = []
    timer = []
    label = []
    labelcolor = []
    colormap = []
    colorbar = []
    true_sink = []
    streamlines = []
    scale_l = []
    fontsize = []

    for map_num in range(0, int(args.geometry[0])*int(args.geometry[1])):
        map_name = "map%02d" % (map_num+1)

        # load from file
        proj.append(config.getint(map_name, "proj"))
        kind.append(config.get(map_name, "kind"))
        sink_flags.append(config.getboolean(map_name, "sink"))
        logscale.append(config.getboolean(map_name, "logscale"))
        r_min.append(config.get(map_name, "min"))
        r_max.append(config.get(map_name, "max"))
        lenbar.append(config.get(map_name, "bar"))
        if lenbar[map_num]:
            barunit = lenbar[map_num].split(" ")[-1]
            if barunit == 'pc':
                scale_l.append(1e0)
            elif barunit == 'kpc':
                scale_l.append(1e3)
            elif barunit == 'Mpc':
                scale_l.append(1e6)
            elif barunit == 'AU':
                scale_l.append(1./206264.806)
        else:
            scale_l.append(0.)
        timer.append(config.getboolean(map_name, "timer"))
        label.append(config.get(map_name, "label"))
        labelcolor.append(config.get(map_name, "labelcolor"))
        colormap.append(config.get(map_name, "colormap"))
        colorbar.append(config.getboolean(map_name, "colorbar"))
        true_sink.append(config.getboolean(map_name, "true_sink"))
        streamlines.append(config.getboolean(map_name, "streamlines"))
        fontsize.append(config.getint(map_name, "fontsize"))

    args.proj = proj
    args.kind = kind
    args.logscale = logscale
    args.min = r_min
    args.max = r_max
    args.bar = lenbar
    args.timer = timer
    args.label = label
    args.labelcolor = labelcolor
    args.colormap = colormap
    args.colorbar = colorbar
    args.sink_flags = sink_flags
    args.true_sink = true_sink
    args.streamlines = streamlines
    args.geometry = [int(args.geometry[0]), int(args.geometry[1])]
    args.fname = config.get("general", "outputfile")
    args.scale_l = scale_l
    args.fontsize = fontsize

    # override config file values with the command line args
    for k in range(len(sys.argv)):
        if sys.argv[k] in ["-n", "--ncpu"]:
            args.ncpu = int(sys.argv[k+1])
        if sys.argv[k] in ["-o", "--output"]:
            args.fname = sys.argv[k+1]

    return args
