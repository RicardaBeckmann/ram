# README #

### RAM ###

* This is a repository for RAMSES Animation Maker
* Current version: 2.0.7 (August 2018)
* Wiki with examples is here: (https://bitbucket.org/biernacki/ram/wiki/Home)

RAM relies on [ffmpeg](https://ffmpeg.org) to produce movies, please adjust your bin_ffmpeg path in `ram.py`

From version 1.11 RAM requres `f90nml` (https://github.com/marshallward/f90nml.git), which can be installed with `pip install f90nml`.

### RISE ###

RISE, or RAMSES Image SEquencer, is a small extension to RAM. It assembles a sequence of movie images into a single png file, thus can be used in papers. RISE uses the same config as RAM, but extended by small section; see the [RISE wiki](https://bitbucket.org/biernacki/ram/wiki/Home#markdown-header-rise)



### Please, feel free to create issues or request new features!
